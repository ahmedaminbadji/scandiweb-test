<?php
include_once("product.php");

class Furniture extends Product {
    var $height;
    var $width;
    var $length;

    function __construct($sku, $name,$price,$height,$width,$length)
    {
        parent::__construct($sku, $name,$price);
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
    }
    public function add(){
        $pdo = dbConnection();  
        parent::add();
        $query = "INSERT INTO furniture (height,width,length,SKU) VALUES(?,?,?,?)";
        $sql = $pdo->prepare($query);
        return $sql->execute([$this->height,$this->width,$this->length,$this->sku]);
    }
}