<?php
include_once("product.php");

class Book extends Product {
    var $weight;

    function __construct($sku, $name,$price,$weight)
    {
        parent::__construct($sku, $name,$price);
        $this->weight = $weight;
    }
    public function add(){
        $pdo = dbConnection();  
        parent::add();
        $query = "INSERT INTO books (weight,SKU) VALUES(?,?)";
        $sql = $pdo->prepare($query);
        return $sql->execute([$this->weight,$this->sku]);
    }
}