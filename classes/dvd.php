<?php
include_once("product.php");

class DVD extends Product {
    var $size;

    function __construct($sku, $name,$price,$size)
    {
        parent::__construct($sku, $name,$price);
        $this->size = $size;
    }
    public function add(){
        $pdo = dbConnection();  
        parent::add();
        $query = "INSERT INTO dvd (size,SKU) VALUES(?,?)";
        $sql = $pdo->prepare($query);
        return $sql->execute([$this->size,$this->sku]);
    }
}