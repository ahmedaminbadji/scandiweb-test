<?php
include("../config/db.php");
include("../classes/furniture.php");
include("../classes/book.php");
include("../classes/dvd.php");

  if(isset($_POST["typeswitcher"])){

  
    switch ($_POST["typeswitcher"]) {
        case "DVD":
          $dvd = new DVD($_POST["sku"],$_POST["name"],$_POST["price"],$_POST["size"]);
          $dvd->add();
          ?>
            <script>
            window.location.href = "../index.php";
            </script>
          <?php     
          break;
        case "Furniture":
            $furniture = new Furniture($_POST["sku"],$_POST["name"],$_POST["price"],$_POST["height"],$_POST["width"],$_POST["length"]);
            $furniture->add();
            ?>
            <script>
            window.location.href = "../index.php";
            </script>
          <?php            
          break;
        case "Book":
            $book = new Book($_POST["sku"],$_POST["name"],$_POST["price"],$_POST["weight"]);
            $book->add();
            ?>
            <script>
            window.location.href = "../index.php";
            </script>
          <?php      
          break;
        default:
          break;
      } 
    }
