<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <title>Product Add</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <link rel="stylesheet" href="add.css">
        <script
                src="https://code.jquery.com/jquery-3.5.1.min.js"
                integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
                crossorigin="anonymous"></script>
                <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    </head>
    <body>
    <div class="container-fluid main" >
            <div class="row" style="border-bottom: 2px solid black;">
                <div class="mr-auto" style="margin-left:1%;">
                <h1>Product Add</h1>
                </div>
                <div class="ml-auto" style="line-height:40px;margin-top:8px;margin-bottom:8px;margin-right:1%;">
                <button class="btn"  type="submit" form="addProductForm" >Save</button>
                <a href="index.php" class="btn">Cancel</a>
                </div>
            </div>
            <br><br>
            <form action="process/add.php" method="post" id="addProductForm">
            <label for="sku">SKU</label>
            <input type="text" name="sku" required>
            <br>
            <label for="name">Name</label>
            <input type="text" name="name" required>
            <br>
            <label for="price">Price ($)</label>
            <input type="text" name="price" required>
            <br>
            <label for="typeswitcher">Type Switcher</label>
            <select name="typeswitcher" id="typeswitcher" required>
                <option value="" disabled selected>Select type</option>
                <option value="DVD">DVD</option>
                <option value="Book">Book</option>
                <option value="Furniture">Furniture</option>
            </select>
            <br><br>
            <div id="dvd" style="display:none;">
            <label for="size">Size (MB)</label>
            <input type="number" min="0" step="any" name="size">
            <p>Please, provide size</p>
            <br>
            </div>
            <div id="furniture" style="display:none;">
            <label for="height">Height (CM)</label>
            <input type="number" min="0" step="any" name="height">
            <br>
            <label for="width">Width (CM)</label>
            <input type="number" min="0" step="any" name="width" >
            <br>
            <label for="length">Length (CM)</label>
            <input type="number" min="0" step="any" name="length" >
            <p>Please, provide dimensions</p>
            <br>
            </div>
            <div id="book" style="display:none;">
            <label for="weight">Weight (KG)</label>
            <input type="number" min="0" step="any" name="weight">
            <p>Please, provide weight”</p>
            <br>
            </div>

            </form>

            <br><br><br>
            <div class="footer">
                <center>
                Scandiweb Test assignment
                </center>
            </div>
    </div>
    <script>
        $("#typeswitcher").change(function(){
            switch($("#typeswitcher").val()) {
                case "DVD":
                    $("#dvd").css("display","block");
                    $("#furniture").css("display","none");
                    $("#book").css("display","none");
                    $('input[name ="size"]').prop('required',true);
                    $('input[name ="height"]').prop('required',false);
                    $('input[name ="width"]').prop('required',false);
                    $('input[name ="length"]').prop('required',false);
                    $('input[name ="weight"]').prop('required',false);
                    break;
                case "Book":
                    $("#dvd").css("display","none");
                    $("#furniture").css("display","none");
                    $("#book").css("display","block");
                    $('input[name ="size"]').prop('required',false);
                    $('input[name ="height"]').prop('required',false);
                    $('input[name ="width"]').prop('required',false);
                    $('input[name ="length"]').prop('required',false);
                    $('input[name ="weight"]').prop('required',true);
                    break;
                case "Furniture":
                    $("#dvd").css("display","none");
                    $("#furniture").css("display","block");
                    $("#book").css("display","none");
                    $('input[name ="size"]').prop('required',false);
                    $('input[name ="height"]').prop('required',true);
                    $('input[name ="width"]').prop('required',true);
                    $('input[name ="length"]').prop('required',true);
                    $('input[name ="weight"]').prop('required',false);
                    break;
                default:
                    $("#dvd").css("display","none");
                    $("#furniture").css("display","none");
                    $("#book").css("display","none");
                    $('input[name ="size"]').prop('required',false);
                    $('input[name ="height"]').prop('required',false);
                    $('input[name ="width"]').prop('required',false);
                    $('input[name ="length"]').prop('required',false);
                    $('input[name ="weight"]').prop('required',false);
                } 
                        });
        
    </script>
    </body>
</html>