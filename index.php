<?php
include("config/db.php");
$pdo = dbConnection();  
?>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <title>Product List</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <link rel="stylesheet" href="main.css">
        <script
                src="https://code.jquery.com/jquery-3.5.1.min.js"
                integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
                crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    </head>
    <body>
   
        <div class="container-fluid main" >

        <!-- header -->
            <div class="row" style="border-bottom: 2px solid black;">
                <div class="mr-auto" style="margin-left:1%;">
                <h1>Product List</h1>
                </div>
                <div class="ml-auto" style="line-height:40px;margin-top:8px;margin-bottom:8px;margin-right:1%;">
                <a href="add.php" class="btn">ADD</a>
                <button type="submit" form="productsForm" class="btn">MASS DELETE</button>
                </div>
            </div>


            <br><br>
            <form action="process/delete.php" method="post" id="productsForm">
            <?php
            $i= 0;
            $query = "SELECT * FROM `dvd` join products on dvd.SKU = products.SKU";
            $query2 = "SELECT * FROM `furniture` join products on furniture.SKU = products.SKU";
            $query3 = "SELECT * FROM `books` join products on books.SKU = products.SKU";
            $sql = $pdo->prepare($query);   
            $sql2 = $pdo->prepare($query2);   
            $sql3 = $pdo->prepare($query3);  
            $result = $sql->execute();   
            $result2 = $sql2->execute();   
            $result3 = $sql3->execute();   
            while($row = $sql->fetch(PDO::FETCH_ASSOC)){
                if($i==0){
                    //if it is the start of a row (4 products per row)
                    ?>
                    <div class="row">
                    <?php
                }
                ?>
                <div class="col-md-3 col-xs-12">
                <div class="card">
                    <input type="checkbox" name="products[]" value="<?php echo $row['SKU']; ?>">
                    <div class="card-text">
                        <b> <?php echo $row["SKU"]; ?> </b>
                        <br>
                        <b><?php echo $row["name"]; ?></b>
                        <br>
                        <b><?php echo $row["price"]; ?> $</b>
                        <br>
                        <b>Size : <?php echo $row["size"]; ?> MB</b>
                    </div>
                </div>
            </div>
            <?php
            $i++;
            if($i == 4 ){
                $i=0;
                ?>
                </div>
                <br>
                <?php
                }
            }
            if($i != 0 ){
                $i=0;
                ?>
                </div>
                <br>
                <?php
            }



            
            while($row = $sql3->fetch(PDO::FETCH_ASSOC)){
                if($i==0){
                    ?>
                    <div class="row">
                    <?php
                }
                ?>
                <div class="col-md-3 col-xs-12">
                <div class="card">
                    <input type="checkbox" name="products[]" value="<?php echo $row['SKU']; ?>">
                    <div class="card-text">
                        <b> <?php echo $row["SKU"]; ?> </b>
                        <br>
                        <b><?php echo $row["name"]; ?></b>
                        <br>
                        <b><?php echo $row["price"]; ?> $</b>
                        <br>
                        <b>Weight : <?php echo $row["weight"]; ?> KG</b>
                    </div>
                </div>
            </div>
            <?php
            $i++;
            if($i == 4 ){
                $i=0;
                ?>
                </div>
                <br>
                <?php
                }
            }
            if($i != 0 ){
                $i=0;
                ?>
                </div>
                <br>
                <?php
            }

            while($row = $sql2->fetch(PDO::FETCH_ASSOC)){
                if($i==0){
                    ?>
                    <div class="row">
                    <?php
                }
                ?>
                <div class="col-md-3 col-xs-12">
                <div class="card">
                    <input type="checkbox" name="products[]" value="<?php echo $row['SKU']; ?>">
                    <div class="card-text">
                        <b> <?php echo $row["SKU"]; ?> </b>
                        <br>
                        <b><?php echo $row["name"]; ?></b>
                        <br>
                        <b><?php echo $row["price"]; ?> $</b>
                        <br>
                        <b>Dimension : <?php echo $row["height"] ."x".$row["width"]."x".$row["length"] ; ?> </b>
                    </div>
                </div>
            </div>
            <?php
            $i++;
            if($i == 4 ){
                $i=0;
                ?>
                </div>
                <br>
                <?php
                }
            }
            if($i != 0 ){
                $i=0;
                ?>
                </div>
                <br>
                <?php
            }
            ?>
            
               
                
                
                
            </form>       
           
            <br><br><br>
            <div class="footer">
                <center>
                Scandiweb Test assignment
                </center>
            </div>
        </div>
    </body>
</html>
